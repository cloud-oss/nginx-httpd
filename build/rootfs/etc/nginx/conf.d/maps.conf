# Add X-Frame-Options for HTML documents.
map $sent_http_content_type $x_frame_options {
    ~*text/html DENY;
}

# Add Content-Security-Policy for HTML documents.
map $sent_http_content_type $content_security_policy {
    ~*text/(html|javascript)|application/pdf|xml "default-src 'self'; base-uri 'none'; form-action 'self'; frame-ancestors 'none'; object-src 'none'; upgrade-insecure-requests";
}

# Add Permissions-Policy for HTML documents.
map $sent_http_content_type $permissions_policy {
    ~*text/(html|javascript)|application/pdf|xml "accelerometer=(),autoplay=(),camera=(),display-capture=(),document-domain=(),encrypted-media=(),fullscreen=(),geolocation=(),gyroscope=(),magnetometer=(),microphone=(),midi=(),payment=(),picture-in-picture=(),publickey-credentials-get=(),screen-wake-lock=(),sync-xhr=(self),usb=(),web-share=(),xr-spatial-tracking=()";
}

# Add Referrer-Policy for HTML documents.
map $sent_http_content_type $referrer_policy {
    ~*text/(css|html|javascript)|application\/pdf|xml "strict-origin-when-cross-origin";
}

# Add Cross-Origin-Policies for HTML documents.
map $sent_http_content_type $coep_policy {
    ~*text/(html|javascript)|application/pdf|xml "require-corp";
}

# Cross-Origin-Opener-Policy
map $sent_http_content_type $coop_policy {
    ~*text/(html|javascript)|application/pdf|xml "same-origin";
}

# Cross-Origin-Resource-Policy
map $sent_http_content_type $corp_policy {
    ~*text/(html|javascript)|application/pdf|xml "same-origin";
}

# Add Access-Control-Allow-Origin.
map $sent_http_content_type $cors {
    # Images
    ~*image/                        "*";
    # Web fonts
    ~*font/                         "*";
    ~*application/vnd.ms-fontobject "*";
    ~*application/x-font-ttf        "*";
    ~*application/font-woff         "*";
    ~*application/x-font-woff       "*";
    ~*application/font-woff2        "*";
}

# Add expiration date
map $sent_http_content_type $expires {
  # Default: Fallback
  default                                 1y;

  # Default: No content
  ""                                      off;

  # Specific: Assets
  ~*image/svg\+xml                        1y;
  ~*image/vnd.microsoft.icon              1w;
  ~*image/x-icon                          1w;

  # Specific: Manifests
  ~*application/manifest\+json            1w;
  ~*text/cache-manifest                   epoch;

  # Specific: Data interchange
  ~*application/atom\+xml                 1h;
  ~*application/rdf\+xml                  1h;
  ~*application/rss\+xml                  1h;

  # Specific: Documents
  ~*text/html                             epoch;
  ~*text/markdown                         epoch;
  ~*text/calendar                         epoch;

  # Specific: Other
  ~*text/x-cross-domain-policy            1w;

  # Generic: Data
  ~*json                                  epoch;
  ~*xml                                   epoch;

  # Generic: WebAssembly
  # ~*application/wasm                    1y; # default

  # Generic: Assets
  # ~*application/javascript              1y; # default
  # ~*application/x-javascript            1y; # default
  # ~*text/javascript                     1y; # default
  # ~*text/css                            1y; # default

  # Generic: Medias
  # ~*audio/                              1y; # default
  # ~*image/                              1y; # default
  # ~*video/                              1y; # default
  # ~*font/                               1y; # default
}

# Add Cache-Control.
map $sent_http_content_type $cache_control {
    default                           "public, immutable, stale-while-revalidate";

    # No content
    ""                                "no-store";

    # Manifest files
    ~*application/manifest\+json      "public";
    ~*text/cache-manifest             ""; # `no-cache` (*)

    # Assets
    ~*image/svg\+xml                  "public, immutable, stale-while-revalidate";
    # Data interchange
    ~*application/(atom|rdf|rss)\+xml "public, stale-while-revalidate";
    # Documents
    ~*text/html                       "private, must-revalidate";
    ~*text/markdown                   "private, must-revalidate";
    ~*text/calendar                   "private, must-revalidate";
    # Data
    ~*json                            ""; # `no-cache` (*)
    ~*xml                             ""; # `no-cache` (*)
}
