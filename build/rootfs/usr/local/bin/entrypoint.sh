#!/bin/sh
set -e

# hook scripts
# shellcheck disable=SC3061
if find "/entrypoint.d/" -mindepth 1 -maxdepth 1 -type f -name '*.sh' -print -quit 2>/dev/null | read -r; then
   HOOK_EXIST=true
fi

if [ "$HOOK_EXIST" = "true" ]; then
   echo "[$(date -R)] [ENTRYPOINT] Found hooks"

   find "/entrypoint.d/" -follow -type f -name '*.sh' -print | sort -n | while read -r hook; do
        echo "[$(date -R)] [ENTRYPOINT] Start hook: $hook"
        /bin/sh "$hook"
        done
fi

# go ahead
if [ "$#" -ne 0 ]; then
   exec "$@"
fi
