#!/bin/sh
set -e

COMPRESSABLE_TYPES=$(grep "/" /etc/nginx/conf.d/compression.conf | tr -d ' ' | sort | uniq)

NO_FILES=0
NO_COMPRESSED=0
NO_SKIP=0

# shellcheck disable=SC2044
for ITEM in $(find /var/www -type f -not -name '*.br' -not -name '*.gz')
do
    NO_FILES=$((NO_FILES + 1))

    # set marker if pre-compressed file already exists
    if [ -f "$ITEM".gz ] || [ -f "$ITEM".br ]; then
       SKIP=true
       NO_SKIP=$((NO_SKIP + 1))
    else
       SKIP=false
    fi

    if [ "$SKIP" = "false" ]; then

       # detect mime type of file
       MIMETYPE=$(file --brief --mime-type "$ITEM")

       # check if mimeype belongs to compressable list
       for LINE in $COMPRESSABLE_TYPES
       do
           if [ "$MIMETYPE" = "$LINE" ]; then
              gzip -6 < "$ITEM" > "$ITEM".gz
              brotli -6 "$ITEM" --output="$ITEM".br
              NO_COMPRESSED=$((NO_COMPRESSED + 1))
           fi
       done

    fi

done

echo "Compression done for $NO_COMPRESSED of $NO_FILES files. Skipped $NO_SKIP files"

find /var/www -type d -exec chmod 0755 {} \;
find /var/www -type f -exec chmod 0644 {} \;
