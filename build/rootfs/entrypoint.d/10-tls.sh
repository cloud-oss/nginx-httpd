#!/bin/sh
set -e

BASENAME=$( basename "$0" )

if [ -f "$NGINX_TLS" ] && [ -f "$NGINX_TLS_KEY" ] && [ "$AUTO_TLS" = "false" ]; then
   echo "[$(date -R)] [$BASENAME] Bring your own SSL Certificate"
else
   echo "[$(date -R)] [$BASENAME] Create selfsigned certificate"
   mkdir -p /tmp/tls
   RANDFILE="/tmp/tls/tls.rnd"
   export RANDFILE

   SSL_SAN="DNS:localhost,DNS:nginx-httpd,DNS:$(hostname -s),IP:$(ip route get 1 | awk '{print $NF;exit}')"
   export SSL_SAN

   openssl genpkey -genparam -algorithm EC -pkeyopt ec_paramgen_curve:P-384 -out /tmp/tls/tls.param
   openssl req -x509 -nodes -days 1094 \
               -newkey ec:/tmp/tls/tls.param \
               -subj "/O=Acme Co/CN=Microservice Fake Certificate" \
               -addext "subjectAltName=${SSL_SAN}" \
               -keyout /tmp/tls/tls.key \
               -out /tmp/tls/tls.pem \
               > /dev/null 2>&1
fi
