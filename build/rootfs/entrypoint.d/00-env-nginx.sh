#!/bin/sh
set -e

BASENAME=$( basename "$0" )

# vars required for nginx
echo "[$(date -R)] [$BASENAME] Startup properties"

DEFINED_VARS=$(env | grep -E 'NGINX_|AUTO_')
for ITEM in $DEFINED_VARS
do
   echo "$ITEM"
done
