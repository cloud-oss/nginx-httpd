#!/bin/sh
set -e

# --------------------------------
# Parameters

if [ "$DEBUG" ]; then
   set -ux
fi

# shellcheck disable=SC1091
. "$(pwd)"/build.args

TIMESTAMP=$(date +%Y%m%d.%H%M)
export TIMESTAMP

IMAGE_NAME=${IMAGE_NAME:-nginx-httpd}
export IMAGE_NAME

IMAGE_TAG=${IMAGE_TAG:-$NGINX_VERSION}
export IMAGE_TAG


# --------------------------------
# Run

# generate build-args
BUILD_ARGS=$(while IFS= read -r line; do printf "%s" "--build-arg $line "; done < build.args)
BUILD_OPTS="--no-cache --force-rm --pull"

# build image
if [ -z "$DEBUG" ]; then
   set -ux
fi

# shellcheck disable=SC2086
docker build $BUILD_OPTS $BUILD_ARGS -t "$IMAGE_NAME":"$IMAGE_TAG"-"$TIMESTAMP" .
