## Create a sample container image including a static website
```
docker build --no-cache --force-rm -f Dockerfile -t my-website .
```

## Run the final image
```
docker run -it --rm --env-file ./env -p 8443:8443 --cpus 1.0 --read-only --name my-website my-website:latest
```